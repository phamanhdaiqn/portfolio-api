import express from "express";
import bodyParser from "body-parser";
import mongoose from 'mongoose';
import cors from 'cors';

import menuRouters from './routes/menus.js';

let app = express();
const PORT = 5000

app.set('port', process.env.PORT || PORT);
app.use(bodyParser.json({ limit: "30mb", extended: true }));
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());
app.use('/menus', menuRouters)

app.get('/', (req, res) => {
    res.send(`<h1>Welcome</h1>`)
});

const CONNECTION_STRING = 'mongodb://127.0.0.1:27017/MyPortfolio?directConnection=true&serverSelectionTimeoutMS=2000';
mongoose.connect(CONNECTION_STRING)
    .then(() => app.listen(PORT, () => console.log(`Server running on port ${PORT}`)))
    .catch((error) => console.log(error.message))