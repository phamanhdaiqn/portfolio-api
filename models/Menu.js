import mongoose from "mongoose";

//define model in Mongo DB
const menuSchema = mongoose.Schema({
    icon: String,
    iconColor: String,
    link: String,
    title: String,
    titleColor: String,
    creator: String,
    createdAt: {
        type: Date,
        default: new Date()
    }
});

const MenuModel = mongoose.model('Menu', menuSchema);
export default MenuModel;