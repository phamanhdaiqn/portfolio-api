import Mongoose from "mongoose";
import MenuModel from "../models/Menu.js";

export const getMenus = async (req, res) => {
    try {
        const menus = await MenuModel.find();
        res.status(200).json(menus);
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
}

export const getAllMenuLinks = async (req, res) => {
    try {
        const menus = await MenuModel.find();
        const result = menus.map((m) => m.link);
        res.status(200).json(result);
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
}

export const getMenuDataByLink = async (req, res) => {
    const { link } = req.params

    try {
        const menu = await MenuModel.findOne({ link: link });
        console.log(menu)
        res.status(200).json(menu);
    } catch (error) {
        res.status(404).json({ error: error.message });
    }
}