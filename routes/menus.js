import express from "express";
import { getMenus, getAllMenuLinks, getMenuDataByLink } from "../controllers/menus.js";

const router = express.Router();

router.get('/', getMenus);
router.get('/links', getAllMenuLinks);
router.get('/:link', getMenuDataByLink);

export default router;